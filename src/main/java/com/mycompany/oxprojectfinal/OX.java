/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.oxprojectfinal;

import java.util.Scanner;

/**
 *
 * @author Arthi
 */
public class OX {

    static String winner = "Draw";
    static char turn = 'X';
    static boolean finish = false;
    static String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    static Scanner kb = new Scanner(System.in);
    static int row=0, col=0;

    public static void main(String[] args) {
        showWelcome();
        while (finish == false) {
            showTable();
            showTurn();
            inputRowCol();
            Process();
            CheckCondition();
        }
        printWinner();
    }

    private static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c] + " ");
            }
            System.out.println(" ");
        }
    }

    private static void showTurn() {
        System.out.println("turn " + turn);
        System.out.println("Please input row, col: ");
    }

    private static void inputRowCol() {
        row = kb.nextInt();
        col = kb.nextInt();
    }

    private static void Process() {
        try {
            if (table[row - 1][col - 1].equals("-") && turn == 'O') {
                table[row - 1][col - 1] = "O";
                turn = 'X';
            } else if (table[row - 1][col - 1].equals("-") && turn == 'X') {
                table[row - 1][col - 1] = "X";
                turn = 'O';
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error!! Please input again.");
        }
    }

    private static boolean CheckHorizontal() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0].equals(table[i][1]) && table[i][1].equals(table[i][2]) && !table[i][0].equals("-")) {
                winner = table[i][0];
                return true;
            }
        }
        return false;
    }

    private static boolean CheckVertical() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i].equals(table[1][i]) && table[1][i].equals(table[2][i]) && !table[0][i].equals("-")) {
                winner = table[0][i];
                return true;
            }
        }
        return false;
    }

    private static boolean CheckLeftDiagonal() {
        if (table[0][0].equals(table[1][1]) && table[1][1].equals(table[2][2]) && !table[1][1].equals("-")) {
            winner = table[0][0];
            return true;
        }
        return false;
    }

    private static boolean CheckRightDiagonal() {
        if (table[0][2].equals(table[1][1]) && table[1][1].equals(table[2][0]) && !table[0][2].equals("-")) {
            winner = table[0][2];
            return true;
        }
        return false;
    }

    private static int CheckDraw() {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!table[i][j].equals("-")) {
                    count++;
                }
            }
        }
        return count;
    }

    private static void CheckCondition() {
        if (CheckHorizontal()) {
            finish = true;
            showTable();
        } else if (CheckVertical()) {
            finish = true;
            showTable();
        } else if (CheckLeftDiagonal()) {
            finish = true;
            showTable();
        } else if (CheckRightDiagonal()) {
            finish = true;
            showTable();
        } else if (CheckDraw() == 9) {
            finish = true;
            showTable();
        }

    }

    private static void printWinner() {
        if (winner.equals("X")) {
            System.out.println(">>>X Win<<<");
        } else if (winner.equals("O")) {
            System.out.println(">>>O Win<<<");
        } else {
            System.out.println(">>>Draw<<<");
        }
    }

}
